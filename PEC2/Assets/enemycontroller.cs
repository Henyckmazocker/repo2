﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class enemycontroller : MonoBehaviour
{
    public float runSpeed = 40f;
    float horizontalmove = -1f;
    public LayerMask platforms;
    //////////////////////esta variable indica en que direccion se mueve el enemigo, -1 izquierda +1 derecha
    float horizontalmovee = 0f;
	private Rigidbody2D m_Rigidbody2D;
	[Range(0, .3f)] [SerializeField] private float m_MovementSmoothing = .05f;
	private bool m_FacingRight = true;
    private Vector3 m_Velocity = Vector3.zero;
    private bool jump = false;
    public float fallmultiplier = 2.5f;
    public float littlejump = 2f;
    public bool choocado = false;

    public GameObject player;
    private int directioncol = -1;

    void Start()
    {
        		m_Rigidbody2D = GetComponent<Rigidbody2D>();

    }

    void Update()
    {



        horizontalmovee = horizontalmove * runSpeed;

        ///////////////Le damos velocidad al enemigo

        if(m_Rigidbody2D.velocity.y < 0){

            m_Rigidbody2D.velocity += Vector2.up * Physics2D.gravity.y * (fallmultiplier -1) * Time.deltaTime;

        }else if(m_Rigidbody2D.velocity.y > 0 && !Input.GetButton("Jump")){

            m_Rigidbody2D.velocity += Vector2.up * Physics2D.gravity.y * (littlejump -1) * Time.deltaTime;

        }
    }

    private void FixedUpdate() {

        ///////////////////////////Si el enemigo colisiona con una pared cambiara de dirección de movimiento.

        chocado();

        if(choocado){
            horizontalmove = horizontalmove * -1f;
            choocado = false;
        }
        
        Vector3 targetVelocity = new Vector2((horizontalmovee*Time.fixedDeltaTime) * 10f, m_Rigidbody2D.velocity.y);
		
        m_Rigidbody2D.velocity = Vector3.SmoothDamp(m_Rigidbody2D.velocity, targetVelocity, ref m_Velocity, m_MovementSmoothing);

			if (horizontalmovee > 0 && !m_FacingRight)
			{
				Flip();
			}
			else if (horizontalmovee < 0 && m_FacingRight)
			{
				Flip();
			}
		

        if (jump == true)
        {

            m_Rigidbody2D.AddForce(new Vector2(0f, 800f));

            jump = false;
        }

    }

    private void Flip()
	{
		m_FacingRight = !m_FacingRight;

		Vector3 theScale = transform.localScale;
		theScale.x *= -1;
		transform.localScale = theScale;
	}

    //////////////////////////Comprobams que el enemigo choca con una pared para cambiar su dirección o choca con el personaje, lo que supone gameover

    void chocado(){

        RaycastHit2D raycast;
          
        if(directioncol == -1){

            raycast= Physics2D.BoxCast(GetComponent<BoxCollider2D>().bounds.center, GetComponent<BoxCollider2D>().bounds.size, 0f, Vector2.left, 0.5f, platforms);

        }else{

            raycast= Physics2D.BoxCast(GetComponent<BoxCollider2D>().bounds.center, GetComponent<BoxCollider2D>().bounds.size, 0f, Vector2.right, 0.5f, platforms);

        }
        if(raycast.collider != null){

            if(raycast.collider.gameObject == player){

                player.SetActive(false);
                SceneManager.LoadScene("endgame");

            }

            directioncol = directioncol * -1;
            choocado = true;
        }else{
            choocado = false;
        }
     }
}
