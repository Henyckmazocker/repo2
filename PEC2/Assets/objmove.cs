﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class objmove : MonoBehaviour
{

    public LayerMask platforms;
    public GameObject enemigo;
    public float runSpeed = 40f;
    //////////////////////esta variable indica en que direccion se mueve el personaje, -1 izquierda +1 derecha
    float horizontalmove = 0f;
	private Rigidbody2D m_Rigidbody2D;
	[Range(0, .3f)] [SerializeField] private float m_MovementSmoothing = .05f;
	private bool m_FacingRight = true;
    private Vector3 m_Velocity = Vector3.zero;
    private bool jump = false;
    public float fallmultiplier = 2.5f;
    public float littlejump = 2f;
    public SpriteRenderer sp;
    private bool firstseen = false;
    private bool groounded = true;

    void Start()
    {
        		m_Rigidbody2D = GetComponent<Rigidbody2D>();

                sp = GetComponent<SpriteRenderer>();

    }

    void Update()
    {

        horizontalmove = Input.GetAxisRaw("Horizontal") * runSpeed;
        
        grounded();

        ///////////////Detectamos si se está apretando la tecla de salto: space

        if(Input.GetButtonDown("Jump") && groounded){
            jump = true;
            groounded = false;
        }

        ///////////////////le damos velocidad al objeto

        if(m_Rigidbody2D.velocity.y < 0){

            m_Rigidbody2D.velocity += Vector2.up * Physics2D.gravity.y * (fallmultiplier -1) * Time.deltaTime;

        }else if(m_Rigidbody2D.velocity.y > 0 && !Input.GetButton("Jump")){

            m_Rigidbody2D.velocity += Vector2.up * Physics2D.gravity.y * (littlejump -1) * Time.deltaTime;

        }

    }

    private void FixedUpdate() {

            ///////////Comprobamos que el personaje haya sido visto por la camara una primera vez, cuando esto ocurra, si el personaje deja de ser visto por la camara game over.

            if (sp.isVisible == false){
                
                if(firstseen == false){

                }else{

                    gameObject.SetActive(false);
                    SceneManager.LoadScene("endgame");

                }

            }else{
                firstseen = true;
            }

        Vector3 targetVelocity = new Vector2((horizontalmove*Time.fixedDeltaTime) * 10f, m_Rigidbody2D.velocity.y);
		
        m_Rigidbody2D.velocity = Vector3.SmoothDamp(m_Rigidbody2D.velocity, targetVelocity, ref m_Velocity, m_MovementSmoothing);

            ////////////cambiamos la orientación del personaje dependiendo de donde nos movamos.

			if (horizontalmove > 0 && !m_FacingRight)
			{
				Flip();
			}
			else if (horizontalmove < 0 && m_FacingRight)
			{
				Flip();
			}
		
        //////////////////////Si estamos saltando le añadimos una fuerza vertical.

        if (jump == true)
        {
            m_Rigidbody2D.AddForce(new Vector2(0f, 500f));
            jump = false;
        }

    }


    private void Flip()
	{
		m_FacingRight = !m_FacingRight;

		Vector3 theScale = transform.localScale;
		theScale.x *= -1;
		transform.localScale = theScale;
	}


    ///////////////////////En esta función detectamos si el personaje esta tocando el suelo para poder saltar, también detectamos si está pisando a un enemigo.

    void grounded(){
          
       RaycastHit2D raycast= Physics2D.BoxCast(GetComponent<BoxCollider2D>().bounds.center, GetComponent<BoxCollider2D>().bounds.size, 0f, Vector2.down, 0.5f, platforms);

        if(raycast.collider != null){

            if(raycast.collider.gameObject == enemigo){
                enemigo.SetActive(false);
            }

            groounded = true;

        }else{
            groounded = false;
        }
     }

}
