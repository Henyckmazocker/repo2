PEC2- Un juego de plataformas- David Carvajal Abellán.

En esta práctica se ha aprendido a usar los movimientos vectoriales, los colliders y tile palettes.
Para afianzar estos conocimientos se ha desarrollado un pequeño projecto de juego de plataformas, en el que podemos realizar las acciones básicas de movimiento: movernos en dirección izquierda y derecha con las teclas A y D, y saltar para sortear obstaculos con la barra espaciadora.

Para generar el escenario se ha utilizado un grid que se ha podido rellenar facilmente con las tilepalette de unity.

Para detectar colisiones se han utilizado boxcolliders.

También se ha implementado unas mejoras a la funcionalidad de salto para que sea más satisfactoria y gratificante.

En el escenario encontramos un enemigo que se mueve en dirección izquierda hasta que colisiona con una pared, momento en el que cambia e dirección, si este enemigo se choca de frente a nosotros perdemos. Si el jugador cae encima del enemigo este desaparecerá.

La cámara de juego seguirá al jugador en el eje x de la pantalla, esto es así porque si el jugador desaparece de la vista de la cámara cayendo la cámara no le seguirá y se perderá la partida.